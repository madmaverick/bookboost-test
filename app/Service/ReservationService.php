<?php


namespace App\Service;

use App\Models\Customer;
use App\Models\Reservation;
use Illuminate\Support\Arr;

class ReservationService extends MewApiService
{
    const GET_ALL_RESERVATION_URL = '/api/connector/v1/reservations/getAll';
    const ADD_RESERVATIONS_URL = '/api/connector/v1/reservations/add';
    const UPDATE_RESERVATIONS_URL = '/api/connector/v1/reservations/update';
    const CONFIRM_RESERVATION_URL = '/api/connector/v1/reservations/confirm';
    const START_RESERVATION_URL = '/api/connector/v1/reservations/start';
    const PROCESS_RESERVATION_URL = '/api/connector/v1/reservations/process';
    const CANCEL_RESERVATION_URL = '/api/connector/v1/reservations/cancel';

    const SPACE_RESOURCE_DISCRIMINATOR = 'Space';

    public function loadReservations($start, $end, array $serviceIds)
    {
        $requestData = [
            'StartUtc' => $start,
            'EndUtc' => $end,
            'ServiceIds' => $serviceIds,
            'Extent' => [
                'Reservations' => true,
                'ReservationGroups' => true,
                'Customers' => true,
                'Resources' => true,
                'BusinessSegments' => true,
            ],
        ];

        $response = $this->apiClient->post(self::GET_ALL_RESERVATION_URL, $requestData);

        foreach($response->json('Reservations') as $apiReservation){

            $businessSegment = $this->getRelatedElement($response->json('BusinessSegments'), $apiReservation['BusinessSegmentId']);
            $recourse = $this->getRelatedElement($response->json('Resources'), $apiReservation['AssignedResourceId']);
            $apiCustomer = $this->getRelatedElement($response->json('Customers'), $apiReservation['CustomerId']);

            $floor = null;
            $room = null;
            if(!empty($recourse['Data']['Discriminator']) && $recourse['Data']['Discriminator'] == self::SPACE_RESOURCE_DISCRIMINATOR){
                $room = $recourse['Name'];
                $floor = $recourse['Data']['Value']['FloorNumber'];
            }

            $customer = (new Customer())->upload(
                $apiCustomer['Id'],
                $apiCustomer['FirstName'],
                $apiCustomer['LastName'],
                $apiCustomer['Email'],
                $apiCustomer['Phone'],
                $apiCustomer['LanguageCode']
            );

            $reservation = (new Reservation())->upload(
                $apiReservation['Id'],
                $apiReservation['ServiceId'],
                $apiReservation['GroupId'],
                $apiReservation['Number'],
                $apiReservation['State'],
                $apiReservation['AssignedResourceId'],
                $businessSegment['Name'],
                $apiReservation['StartUtc'],
                $apiReservation['EndUtc'],
                $apiReservation['CancelledUtc'],
                $apiReservation['CreatedUtc']
            );

            $reservation->customers()->sync([$customer->getKey() => ['room' => $room, 'floor' => $floor]]);
        }
    }

    /**
     * @param array $elements
     * @param $relatedId
     * @return mixed
     */
    private function getRelatedElement(array $elements, $relatedId)
    {
        return Arr::first($elements, function($value) use ($relatedId) {
            if($value['Id'] == $relatedId){
                return $value;
            }
        });
    }

    public static function getServices()
    {
        dd(Http::withOptions(['verify' => false])
            ->post(config('services.mews.domain').'/api/connector/v1/services/getAll', [
                'ClientToken' => config('services.mews.client_token'),
                'AccessToken' => config('services.mews.access_token'),
                'Client' => config('services.mews.client'),
            ])->json());
    }

}