<?php


namespace App\Service;


class CustomerService extends MewApiService
{
    const GET_ALL_CUSTOMERS_URL = '/api/connector/v1/customers/getAll';
    const SEARCH_CUSTOMERS_URL = '/api/connector/v1/customers/search';
    const ADD_CUSTOMERS_URL = '/api/connector/v1/customers/add';
    const UPDATE_CUSTOMERS_URL = '/api/connector/v1/customers/update';

    public function loadCustomers()
    {
        $requestData = [
            'Extent' => [
                'Customers' => true,
                'Documents' => true,
                'Addresses' => false
            ],
        ];

        $response = $this->apiClient->post(self::GET_ALL_CUSTOMERS_URL, $requestData);
    }

}