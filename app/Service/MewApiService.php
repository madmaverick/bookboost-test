<?php


namespace App\Service;


abstract class MewApiService
{
    /**
     * @var MewServiceClient
     */
    protected $apiClient;

    /**
     * MewApiService constructor.
     */
    public function __construct()
    {
        $this->apiClient = new MewServiceClient;
    }

}