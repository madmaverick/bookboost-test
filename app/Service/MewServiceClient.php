<?php


namespace App\Service;


use Illuminate\Support\Facades\Http;

class MewServiceClient
{
    /**
     * @var array
     */
    private $apiCredentials = [];

    /**
     * MewServiceClient constructor.
     */
    public function __construct()
    {
        $this->apiCredentials = [
            'ClientToken' => config('services.mews.client_token'),
            'AccessToken' => config('services.mews.access_token'),
            'Client' => config('services.mews.client'),
        ];
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function setClientCredentials(array $requestData)
    {
        return array_merge($this->apiCredentials, $requestData);
    }

    /**
     * @param $url
     * @param array $requestData
     * @return \Illuminate\Http\Client\Response
     * @throws \Exception
     */
    public function post($url, array $requestData)
    {
        try{
            $response = Http::withOptions(['verify' => false])
                ->post(
                    config('services.mews.domain') . $url,
                    $this->setClientCredentials($requestData)
                );
            return $response;
        }catch (\Exception $exp){
            throw new \Exception($exp->getMessage());
        }
    }
}