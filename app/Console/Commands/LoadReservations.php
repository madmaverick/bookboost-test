<?php

namespace App\Console\Commands;

use App\Service\ReservationService;
use Illuminate\Console\Command;

class LoadReservations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reservation:load {start} {end} {service_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loads reservations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $serviceIds = explode(',', $this->argument('service_id'));

        (new ReservationService)->loadReservations(
            $this->argument('start'),
            $this->argument('end'),
            $serviceIds
        );

        $this->info('Success');
    }
}
