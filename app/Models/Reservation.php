<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'external_id', 'service_id', 'group_id', 'number', 'status', 'source',
        'segment', 'start', 'end', 'cancelled_at', 'created_at_in_pms',
    ];

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'customer_reservation')
            ->withTimestamps()
            ->withPivot(['room', 'floor'])
            ->using(CustomerReservation::class);
    }

    public function upload(
        $external_id,
        $service_id,
        $group_id,
        $number,
        $status,
        $source,
        $segment,
        $start,
        $end,
        $cancelled_at,
        $created_at_in_pms
    )
    {
        return self::firstOrCreate(
            ['external_id' => $external_id],
            [
                'external_id' => $external_id,
                'service_id' => $service_id,
                'group_id' => $group_id,
                'number' => $number,
                'status' => $status,
                'source' => $source,
                'segment' => $segment,
                'start' => Carbon::parse($start)->toDateTime(),
                'end' => Carbon::parse($end)->toDateTime(),
                'cancelled_at' => Carbon::parse($cancelled_at)->toDateTime(),
                'created_at_in_pms' => Carbon::parse($created_at_in_pms)->toDateTime()
            ]
        );
    }
}
