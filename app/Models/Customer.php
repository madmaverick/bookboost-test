<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['external_id', 'first_name', 'last_name', 'email', 'phone', 'locale'];

    public function reservations()
    {
        return $this->belongsToMany(Reservation::class, 'customer_reservation')
            ->withTimestamps()
            ->withPivot(['room', 'floor'])
            ->using(CustomerReservation::class);
    }

    public function upload(
        $external_id,
        $first_name,
        $last_name,
        $email,
        $phone,
        $locale
    )
    {
        return self::firstOrCreate(
            ['external_id' => $external_id],
            [
                'external_id' => $external_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'phone' => $phone,
                'locale' => $locale
            ]
        );
    }
}
