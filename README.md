bookboost-test

requirements:
- composer
- PHP 8.0


1. create DB bookboost_db
2. composer install
3. copy .env.example to .env
4. run command: php artisan migrate
4. run command: php artisan reservation:load 2021-09-27 2021-09-29 bd26d8db-86da-4f96-9efc-e5a4654a4a94

p.s. The required option for script is service_id = bd26d8db-86da-4f96-9efc-e5a4654a4a94 - this is id of Accomodation service
