<?php

namespace Tests\Feature;

use App\Models\Customer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_customer_upload()
    {
        $fake_external_id = 'aaa123-test123';

        (new Customer())->upload(
            $fake_external_id,
            'Jane',
            'Doe',
            'janedoe24@gmail.com',
            '039475243',
            'fr-FR'
        );

        $this->assertNotNull(Customer::where('external_id',$fake_external_id)->first());

        (new Customer())->upload(
            $fake_external_id,
            'Ken',
            'Foster',
            'kenfoster24@gmail.com',
            '0394743456',
            'en-US'
        );

        $this->assertEquals(1,Customer::where('external_id',$fake_external_id)->count());
    }
}
