<?php

namespace Tests\Feature;

use App\Service\MewServiceClient;
use App\Service\ReservationService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoadReservationsTest extends TestCase
{
    use RefreshDatabase;

    public function test_retrieving_data_from_mews_api()
    {
        $accomodationServiceId = 'bd26d8db-86da-4f96-9efc-e5a4654a4a94';

        $requestData = [
            'StartUtc' => '2021-09-27',
            'EndUtc' => '2021-09-29',
            'ServiceIds' => [$accomodationServiceId],
            'Extent' => [
                'Reservations' => true,
                'ReservationGroups' => true,
                'Customers' => true,
                'Resources' => true,
                'BusinessSegments' => true,
            ],
        ];

        $response = (new MewServiceClient())->post(ReservationService::GET_ALL_RESERVATION_URL, $requestData);

        $this->assertEquals(200, $response->status());
    }
}
