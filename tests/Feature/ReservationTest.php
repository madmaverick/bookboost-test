<?php

namespace Tests\Feature;

use App\Models\Reservation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ReservationTest extends TestCase
{
    use RefreshDatabase;

    public function test_reservation_upload()
    {
        $fake_external_id = 'aaa123-test123';

        (new Reservation())->upload(
            $fake_external_id,
            'sskd345-2345ksfd4',
            'testskd-2453lsd-245sdf',
            '12345',
            'Requested',
            'sadf345-5353',
            'Business',
            '2021-09-28T13:00:00Z',
            '2021-09-30T09:00:00Z',
            '2021-09-27T09:00:00Z',
            '2021-09-26T13:00:00Z'
        );

        $this->assertNotNull(Reservation::where('external_id',$fake_external_id)->first());

        (new Reservation())->upload(
            $fake_external_id,
            'bskd-4300skd-skdfj',
            'asdfsdf-34adjs-askdf',
            '54321',
            'Started',
            'asdf334-sdf4322',
            'Business',
            '2021-09-28T13:00:00Z',
            '2021-09-30T09:00:00Z',
            '2021-09-27T09:00:00Z',
            '2021-09-26T13:00:00Z'
        );

        $this->assertEquals(1, Reservation::where('external_id',$fake_external_id)->count());
    }
}
