<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('external_id');
            $table->string('service_id');
            $table->string('group_id')->nullable();
            $table->string('number')->comment('confirmation number');
            $table->string('status');
            $table->string('source')->nullable();
            $table->string('segment')->nullable();
            $table->dateTimeTz('start');
            $table->dateTimeTz('end');
            $table->dateTimeTz('cancelled_at');
            $table->dateTimeTz('created_at_in_pms');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
